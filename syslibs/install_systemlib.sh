#!/bin/bash

sudo apt-get update && apt-get install -y \
    libxml2 \
    libsecret-1-0 \
    libgdal26 \
    libudunits2-0 \
    libpq-dev
